<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('/', 'FrontController@indexof');
    Route::get('/post/{slug}', 'FrontController@post');
    Route::get('/knowledge/{slug}', 'FrontController@kb');

    Route::group(['middleware' => 'guest'], function () {
        Route::get('/admin/resman', 'AdminController@getLogin')->name('login');
        Route::post('/admin/resman', 'AdminController@postLogin')->name('post.login'); 
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'auth', ], function () {
        Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
        Route::get('/knowledge', 'KbController@index')->name('knowledge');
        Route::resource('/knowledge', 'KbController');
        Route::get('/section', 'SectionController@index')->name('section');
        Route::get('/artikel', 'ArtikelController@index')->name('artikel');
        Route::resource('/artikel', 'ArtikelController');
        Route::get('/galeri', 'GaleriController@index')->name('galeri');
        Route::get('/keluhan', 'KeluhanController@index')->name('keluhan');
        Route::get('/permintaan', 'PermintaanController@index')->name('permintaan');
        Route::get('/master_rs', 'RsController@index')->name('rs');
        Route::get('/setting', 'AdminController@setting')->name('setting');
        Route::view('knowledge/get', 'livewire.kb-index');
        Route::view('knowledge/post', 'livewire.kb-create');
        Route::view('knowledge/put', 'livewire.kb-update');

        Route::view('artikel/get', 'livewire.post.index');
        Route::view('artikel/post', 'livewire.post.create');
        Route::view('artikel/put', 'livewire.post.update');


        Route::view('/test', 'livewire.partials.model');
        Route::get('/test', function(){
            return str_random(32);
        });
    });

    Route::get('/logout', 'AdminController@logout')->name('logout');
    Route::group(['prefix' => '{language}'], function(){

    Route::get('/', 'FrontController@index');
    Route::get('/blog', 'FrontController@blog');
    Route::get('/keluhan', 'FrontController@getKeluhan')->name('keluhan');
    Route::get('/blog', 'FrontController@blog');
    Route::get('/kb', 'FrontController@knowledge');
    Route::get('/about', 'FrontController@getAbout')->name('about');

});
