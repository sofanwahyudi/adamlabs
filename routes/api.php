<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('keluhan', function () {
    app()->setlocale('id');
    $res = Http::get('https://jsonplaceholder.typicode.com/posts');
    return [$res->body(), $res->status()];
});

Route::post('keluhan', function(Request $request){
    app()->setlocale('id');

    $data['status'] = "Sukses";
   $res = ['ip' => $request->ip, 'pic' => $request->pic, 'keluhan' => $request->keluhan,];
   //dd($res);
     \App\Model\Keluhan::create(['ip' => $request->ip, 'pic' => $request->pic, 'keluhan' => $request->keluhan,]);
    return response($data, 201); 

})->name('testapi');

