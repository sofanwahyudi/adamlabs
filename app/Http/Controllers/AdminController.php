<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use App\User;


class AdminController extends Controller
{
    public function dashboard()
    {
    return view('admin.dashboard');  
    }

    public function setting()
    {

        return view('admin.setting');
    }
    public function getLogin(Request $request)
    {
        return view('admin.login');
    }
    public function postLogin(Request $request)
    {
        if (!Auth::attempt(['email'=> $request->email, 'password'=> $request->password])) {
            return redirect()->back()->with('error', 'Maaf, Email atau Password Salah');
        }
        return redirect()->route('dashboard');
    }
    public function logout()
    {
        $user = Auth::logout();
        return redirect()->route('login');
    }
}
