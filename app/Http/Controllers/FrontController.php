<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Post;
use App\Model\Kb;

class FrontController extends Controller
{
    public function indexof()
    {
        app()->setlocale('id');
        $blog = Post::latest()->get()->take(3);
        return view('welcome')->withBlog($blog);
    }
    public function index($lang){
        
        \App::getLocale($lang);
        $blog = Post::latest()->where('status', 'published')->get()->take(3);
        return view('welcome')->withBlog($blog);
    }
    public function blog($lang)
    {
        \App::getLocale('id');
        $blog = Post::where('status', 'published')->get();
        return view('blog')->withBlog($blog);
    }

    public function post($slug)
    {
        $post = Post::where('id', $slug)->where('status', 'published')->orWhere('slug', $slug)->firstOrFail();
        // dd($blog);
        return view('post')->withPost($post);
    }
    public function getKeluhan($lang)
    {
        \App::getLocale($lang);
        return view('keluhan');
    }
    public function getPermintaan($lang)
    {
        \App::getLocale($lang);
        return view('permintaan');
    }
    public function getAbout($lang)
    {
        \App::getLocale($lang);
        return view('about');
    }
    public function knowledge($lang)
    {
        \App::getLocale($lang);
        $kb = Kb::where('status', 'published')->get();
        return view('knowledge')->withKb($kb);
    }
    public function kb($slug)
    {
        $kb = Kb::where('id', $slug)->orWhere('slug', $slug)->firstOrFail();
        return view('kbdetail')->withKb($kb);
    }
}
