<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Model\Kb;
use Livewire\WithPagination;

class KbIndex extends Component
{
    use WithPagination; 
    public $statusUpdate = false;
    public $paginate = 5;
    public $search;
    public $refresh;
    public $confirming;
    public $publish = 'published';
    public $draft = 2;

    protected $listeners =[
        'kbStored' => 'handleStored',
        'kbUpdated' => 'handleUpdated',
    ];
    public function render()
    {
        return view('livewire.kb-index', [
            'kb' => $this->search === null ? Kb::latest()->paginate($this->paginate) : Kb::latest()->where('title', 'like', '%'. $this->search . '%')->paginate($this->paginate)
        ]);
    }
    public function refresh()
    {
        $this->statusUpdate = false;
        return view('livewire.kb-index');
    }

    public function edit($id)
    {
        $this->statusUpdate = true;
        $knowledge = Kb::find($id);
        $this->emit('edit', $knowledge);
    }

    public function handleStored($kb)
    {
        session()->flash('message', 'Data Berhasil Disimpan 😁');
    }
    public function handleUpdated($kb)
    {
        session()->flash('message', 'Data Berhasil Diupdate 😁');
    }
    public function draftKb($id)
    {
        if ($id) {
            $data = Kb::findOrFail($id);
            $data->status = 'drafted';
            $data->save();
       
            session()->flash('message', 'Data Berhasil Diarsipkan 😁');
        }
    }
    public function publishKb($id)
    {
        if ($id) {
            $data = Kb::findOrFail($id);
            $data->status = 'published';
            $data->save();
       
            session()->flash('message', 'Data Berhasil Dipublish 😁');
        }
    }
    public function confirmDelete($id)
    {
        $this->confirming = $id;
    }
    public function hapus($id)
    {
        if ($id) {
            $data = Kb::findOrFail($id);
            $data->delete();
       
            session()->flash('message', 'Data Berhasil Dihapus 😁');
        }
    }
}
