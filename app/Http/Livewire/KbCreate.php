<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Model\Kb;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Auth;
use App\Model\Kategori;


class KbCreate extends Component
{
    use WithFileUploads;
    public $title;
    public $content;
    public $image;
    public $kategori;
    public $textarea;
    public $cat;

    protected $listeners = ['fileUpload' => 'handleFileUpload', 'textArea' => 'handleTextArea'];

    public function handleFileUpload($imageData)
    {
        $this->image = $imageData;
    }

    public function handleTextArea($textArea)
    {
        $this->textarea = $textArea;
    }

    public function render()
    {
        return view('livewire.kb-create');
    }

    public function store()
    {
        // dd($this->textarea);
        $this->validate([
            'title' => 'required',
            'kategori' => 'required',
            // 'content' => 'required',
          ]);
        $images = ImageManagerStatic::make($this->image)->encode('jpg');
        $filename  = Str::random() . '.jpg';
        Storage::disk('public')->put($filename, $images);
        $slug = str_slug($this->title);
        $content = $this->textarea;
      //  dd($slug);
      $user = Auth::user()->id;
    Kb::create([
            'title' => $this->title,
            'content' => $content,
            'image' => $filename,
            'kategori_id' => $this->kategori,
            'users_id' => $user,
            'slug' => $slug,
        ]);
             $this->resetField();
             $this->emit('kbStored');
    }
    public function addCat()
    {
        Kategori::create([
            'kategori' => $this->cat
        ]);
        $this->resetField();
        session()->flash('message', 'Data Berhasil Disimpan 😁');
    }

    private function resetField()
    {
        $this->textarea = null;
        $this->title = null;
        $this->content = null;
        $this->image= null;
        $this->cat= null;
    }
}
