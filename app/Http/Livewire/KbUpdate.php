<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Model\Kb;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Auth;

class KbUpdate extends Component
{
    use WithFileUploads;
    public $title;
    public $content;
    public $image;
    public $kategori;
    public $kbId;
    public $textarea;

    protected $listeners =[
        'edit' => 'editKb',
        'fileUpload' => 'handleFileUpload',
        'textArea' => 'handleTextArea'
    ];

    public function handleFileUpload($imageData)
    {
        $this->image = $imageData;
        dd($imageData);
    }
    public function handleTextArea($textArea)
    {
        $this->textarea = $textArea;
    }
    public function render()
    {
        return view('livewire.kb-update');
    }

    public function editKb($knowledge)
    {
       // dd($knowledge);
        $this->title = $knowledge['title'];
        $this->content = $knowledge['content'];
        $this->image = $knowledge['image'];
        $this->kategori = $knowledge['kategori_id'];
        $this->kbId = $knowledge['id'];
    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            'kategori' => 'required',
            'content' => 'required',
            // 'image' => 'required'
          ]);
          $user = Auth::user()->id;
          if($this->kbId){
            $knowledge = Kb::find($this->kbId);
            // if($this->image){
            //     $images = ImageManagerStatic::make($this->image)->encode('jpg');
            //     $filename  = Str::random() . '.jpg';
            //     Storage::disk('public')->put($filename, $images);
            //     $knowledge->update([
            //         'title' => $this->title,
            //         'content' => $this->content,
            //         'kategori_id' => $this->kategori,
            //         'image' => $filename,
            //         'users_id' => $user,
            //     ]);
            // }
            $knowledge->update([
                'title' => $this->title,
                'content' => $this->content,
                'kategori_id' => $this->kategori,
                'users_id' => $user,
            ]);
          }
          $this->resetField();
          $this->emit('kbUpdated');
    }
    private function resetField()
    {
        $this->title = null;
        $this->content = null;
        $this->image= null;
    }
}
