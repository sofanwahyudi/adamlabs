<?php

namespace App\Http\Livewire\Post;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Auth;
use App\Model\Post;
use App\Model\Category;

class Create extends Component
{
    use WithFileUploads;
    public $title;
    public $content;
    public $image;
    public $kategori;
    public $textarea;
    public $clear;
    public $addKat;
    public $kat;
    public $isOpen = false;
   


    protected $listeners = [
        'fileUpload' => 'handleFileUpload',
        'textArea' => 'handleTextArea',
        'clear' => 'resetSummernote',
        'addKat' => 'addKat',
        'showModal' => 'open',
        'closeModal' => 'close',
        ];

    public function open()
    {
        $this->isOpen = true;
    }

    public function close()
    {
        $this->isOpen = false;
    }
    public function handleFileUpload($imageData)
    {
        $this->image = $imageData;
    }

    public function handleTextArea($textArea)
    {
        $this->textarea = $textArea;
    }
    public function addKat()
    {
        if ($this->isOpen) {
            $this->isOpen = false;
        } else {
            $this->isOpen = true;
        }
    }
    public function render()
    {
        return view('livewire.post.create');
    }
    public function store()
    {
       // dd($this->textarea);
        $this->validate([
            'title' => 'required',
            'kategori' => 'required',
            // 'content' => 'required',
          ]);
        $images = ImageManagerStatic::make($this->image)->encode('jpg');
        $filename  = Str::random() . '.jpg';
        Storage::disk('public')->put($filename, $images);
        $slug = str_slug($this->title);
        $content = $this->textarea;
      //  dd($slug);
      $user = Auth::user()->id;
        Post::create([
                'title' => $this->title,
                'content' => $content,
                'image' => $filename,
                'category_id' => $this->kategori,
                'users_id' => $user,
                'slug' => $slug,
            ]);
                $this->resetField();
                $this->resetSummernote();
                $this->emit('postStored');
    }

    public function addKategori()
    {
        // dd($this->kat);
        Category::create([
            'category' => $this->kat
        ]);
        $this->resetField();
        session()->flash('message', 'Data Berhasil Disimpan 😁');

    }

    private function resetField()
    {
        $this->textarea = '';
        $this->title = null;
        $this->content = null;
        $this->image= null;
        $this->kat= null;
    }
    private function resetSummernote()
    {
        $this->textarea = $this->clear;
    }
}
