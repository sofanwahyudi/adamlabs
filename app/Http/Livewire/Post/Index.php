<?php

namespace App\Http\Livewire\Post;

use Livewire\Component;
use App\Model\Post;
use App\User;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination; 
    public $statusUpdate = false;
    public $paginate = 5;
    public $search;
    public $refresh;
    public $confirming;
    public $addKat;
    public $publish = 'published';
    public $draft = 2;

    protected $listeners =[
        'postStored' => 'handleStored',
        'postUpdated' => 'handleUpdated',
    ];
    public function render()
    {
        return view('livewire.post.index', [
            'post' => $this->search === null ? Post::latest()->paginate($this->paginate) : Post::latest()->where('title', 'like', '%'. $this->search . '%')->paginate($this->paginate)
        ]);
    }
    public function refresh()
    {
        $this->statusUpdate = false;
        return view('livewire.post.index');
    }

    public function edit($id)
    {
        $this->statusUpdate = true;
        $post = Post::find($id);
        $this->emit('edit', $post);
    }

    public function handleStored()
    {
        session()->flash('message', 'Data Berhasil Disimpan 😁');
    }
    public function handleUpdated($post)
    {
        session()->flash('message', 'Data Berhasil Diupdate 😁');
    }
    public function draftPost($id)
    {
        if ($id) {
            $data = Post::findOrFail($id);
            $data->status = 'drafted';
            $data->save();
       
            session()->flash('message', 'Data Berhasil Diarsipkan 😁');
        }
    }
    public function publishPost($id)
    {
        if ($id) {
            $data = Post::findOrFail($id);
            $data->status = 'published';
            $data->save();
       
            session()->flash('message', 'Data Berhasil Dipublish 😁');
        }
    }
    public function confirmDelete($id)
    {
        $this->confirming = $id;
    }
    public function hapus($id)
    {
        if ($id) {
            $data = Post::findOrFail($id);
            $data->delete();
       
            session()->flash('message', 'Data Berhasil Dihapus 😁');
        }
    }
}
