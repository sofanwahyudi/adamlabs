<?php

namespace App\Http\Livewire\Post;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Auth;
use App\Model\Post;

class Update extends Component
{
    use WithFileUploads;
    public $title;
    public $content;
    public $image;
    public $kategori;
    public $postId;
    public $textarea;
    protected $listeners =[
        'edit' => 'editPost',
        'fileUpload' => 'handleFileUpload',
        'textArea' => 'handleTextArea'
    ];
    public function handleFileUpload($imageData)
    {
        $this->image = $imageData;
    }
    public function handleTextArea($textArea)
    {
        $this->textarea = $textArea;
    }
    public function render()
    {
        return view('livewire.post.update');
    }
    public function editPost($post)
    {
        $this->title = $post['title'];
        $this->textarea = $post['content'];
        $this->image = $post['image'];
        $this->kategori = $post['category_id'];
        $this->postId = $post['id'];
    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            'kategori' => 'required',
          ]);
          $user = Auth::user()->id;
          if($this->postId){
            $post = Post::find($this->postId);
            // if($this->image){
            //     $images = ImageManagerStatic::make($this->image)->encode('jpg');
            //     $filename  = Str::random() . '.jpg';
            //     Storage::disk('public')->put($filename, $images);
            //     $post->update([
            //         'title' => $this->title,
            //         'content' => $this->content,
            //         'category_id' => $this->kategori,
            //         'image' => $filename,
            //         'users_id' => $user,
            //     ]);
            // }
            $post->update([
                'title' => $this->title,
                'content' => $this->content,
                'category_id' => $this->kategori,
                'users_id' => $user,
            ]);
          }
          $this->resetField();
          $this->emit('postUpdated');
    }
    private function resetField()
    {
        $this->title = null;
        $this->content = null;
        $this->image= null;
        $this->kategori= null;
    }
}
