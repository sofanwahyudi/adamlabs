<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Keluhan extends Model
{
    protected $table='keluhan';
    protected $fillable=['ip', 'pic', 'keluhan'];
}
