<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kb extends Model
{
    protected $table = 'kb';
    protected $fillable = ['title', 'content', 'image', 'users_id', 'kategori_id', 'slug'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    public function kategori()
    {
        return $this->belongsTo('App\Model\Kategori', 'kategori_id');
    }
    public function getGambar(){
        if(!$this->image){
            return asset('assets/img/750x300.png');
        }
        return asset('storage/' .$this->image);
    }
}
