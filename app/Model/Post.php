<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['title', 'content', 'image', 'users_id', 'category_id', 'slug'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    public function categories()
    {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }
    public function getGambar(){
        if(!$this->image){
            return asset('assets/img/750x300.png');
        }
        return asset('storage/' .$this->image);
    }
}
