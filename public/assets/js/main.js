/*
* --------------------------- Main Js File -----------------------------
Template Name : Zein - Personal Portfolio Template
Author        : Seniman Koding
Version       : 1.0
Copyright     : 2019
* ----------------------------------------------------------------------
*/

/*
* ----------------------------------------------------------------------


*/

$(document).ready(function(){

	/* Navbar ( Change Background & Logo )  */

    $(window).on("scroll",function () {

        var bodyScroll = $(window).scrollTop(),
            navbar = $(".navbar"),
            navbarCollapse = $(".navbar-collapse");
            navToggler = $(".navbar-toggler");
            // logo = $(".navbar .navbar-brand> img");

        if(bodyScroll > 100){
            navbar.addClass("nav-scroll");
            navbarCollapse.addClass("nav-collapse");
            navToggler.addClass("nav-toggler");
            // logo.attr('src', 'assets/img/logo-seniman-koding-langit.png');

        }else{
            navbar.removeClass("nav-scroll");
            navbarCollapse.removeClass("nav-collapse");
            navToggler.removeClass("nav-toggler");
            // logo.attr('src', 'assets/img/logo-seniman-koding.png');
        }
    });


        // close navbar-collapse when a  clicked
        $(".navbar-nav a").on('click', function () {
            $(".navbar-collapse").removeClass("show");
        });

    /* End Navbar */


    /* Skil Progres */
    $(window).on('scroll', function () {
        $(".skill-item-progress .progres").each(function () {
            var bottom_of_object = 
            $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = 
            $(window).scrollTop() + $(window).height();
            var myVal = $(this).attr('data-value');
            if(bottom_of_window > bottom_of_object) {
                $(this).css({
                  width : myVal
                });
            }
        });
    });
    /* End Skil Progres */


    /* Work */
    $(function () {
        $('.gallery').magnificPopup({
            delegate: '.popimg',
            type: 'image',
            gallery: {
                enabled: true
            }
        });

        $('.gallery').isotope({
            itemSelector: '.items'
        });

        var $gallery = $('.gallery').isotope({
            // options
        });

        // filter items on button click
        $('.filtering').on( 'click', 'span', function() {
            var filterValue = $(this).attr('data-filter');
                $gallery.isotope({ filter: filterValue });
        });

        $('.filtering').on( 'click', 'span', function() {
            $(this).addClass('active').siblings().removeClass('active');

        });            
    }); 
    /* End Work */


    /* Slider Testimonial */
    var portfolio = $(".pd-carusel");
    portfolio.owlCarousel({
        items: 4,
        margin: 10,
        dots: true,
        autoplay: true,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {items: 1}, 
            500: {items: 1}, 
            1000: {items: 2}, 
            1200: {items: 3}
            }
    });
    /* End Slider Testimonial */


    /* Scroll To Top */
    var win = $(window),
        navbar = $('.navbar'),
        scrollUp = $(".scroll-up");

    win.on('scroll', function () {
        if ($(this).scrollTop() >= 600) {
            scrollUp.show(300);
        } else {
            scrollUp.hide(300);
        }
    });

        // Back To 0 Scroll Top body
        scrollUp.on('click', function () {
            $("html, body").animate({ scrollTop: 0}, 1000);
        });
    /* End Scroll To Top */


    /* Wow Init */
    var wow = new WOW({
            mobile: false
        });
        wow.init();
    /* End Wow Init */


    /* Fitur Page */
    $('.card-header').on('click', function () {
        $('.card-header').removeClass('active');
        $(this).addClass('active');
    });

});