// ambil elemen2 yang dibutuhkan
var keyword = document.getElementById('keyword');
var tombolCari = document.getElementById('tombol-cari');
var container = document.getElementById('container');

keyword.addEventListener('keyup', function() {
	// console.log(keyword.value);

	// buat objeck ajak
	var xhr = new XMLHttpRequest();

	// cek kesiapan ajak
	xhr.onreadystatechange = function() {
		if( xhr.readyState == 4 && xhr.status == 200 ) {
			// console.log(xhr.responseText);
			container.innerHTML = xhr.responseText;
		}
	}

	// eksekusi ajak
	xhr.open('GET', 'assets/searc/search.php?keyword=' + keyword.value, true);
	xhr.send();

});