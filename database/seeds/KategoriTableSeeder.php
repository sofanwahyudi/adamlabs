<?php

use Illuminate\Database\Seeder;
use App\Model\Kategori;
class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            'backend',
            'datalogger',
            'teknis',
            'alat',
            'jaringan',
            'server',
            'klien',
         ];
         foreach ($kategori as $cat) {
            Kategori::create(['kategori' => $cat]);
       }
    }
}
