<?php

use Illuminate\Database\Seeder;
use App\Model\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            'covid-19',
            'kesehatan',
            'kedokteran',
            'pasien',
            'virus',
            'alat',
            'teknologi',
         ];
         foreach ($kategori as $cat) {
            Category::create(['category' => $cat]);
       }
    }
}
