<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use Carbon\Carbon;

class KbTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kb')->truncate();

        //generate 10 dummy posts
        $image = "http://placehold.it/750x300";

        $posts = [];
        $faker = Factory::create();
        $date = Carbon::create(2018, 7, 10, 11);

        for($i=1; $i<=50; $i++){
	        $date->addDays(1);
            $publishedDate = clone($date);
            $createdDate = clone($date);

        	$posts[] = [
                'users_id' => 1,
                'kategori_id' => 1,
        		'title' => $faker->sentence(rand(8,12)),
        		'content' => $faker->paragraphs(rand(10,15), true),
        		'slug' => $faker->slug(),
        		'image' => rand(0,1) == 1 ? $image : null,
        		'created_at' => $createdDate,
                'updated_at' => $createdDate,
        	];
        }
        DB::table('kb')->insert($posts);
    }
}
