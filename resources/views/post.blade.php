@extends('layouts.index')
@section('title')
Post {{$post->title}}
@stop
@section('content')
<section id="blog">
    <div class="container">
        <div class="title-section">
            <div class="title-section-main">
                {{$post->title}}
            </div>
        </div>

        <div class="blog-detail">
            <div class="blog-box">
                <div class="blog-img">
                    <a href="">
                        <img src="{{ $post->getGambar()}}" alt="blog">
                        <span class="blog-img-date">{{ $post->created_at->diffForHumans()}}</span>
                    </a>
                </div>
                <div class="blog-box-title">
                    <a href="">
                        {{$post->title}}
                    </a>
                </div>
                <div class="container">
                    <p>
                        {!! $post->content !!}
                    </p>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection
