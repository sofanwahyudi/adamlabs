@extends('layouts.index')
@section('title')
@lang('trans.welcome') Adamlabs Website    
@endsection
@section('content')
@php $locale = app()->getLocale(); @endphp
<section id="zein-intro-area">
    <div class="container">
        <div class="caption-intro wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
            <div class="row">
                <div class="col-md-6">
                    <div class="caption-intro-desc">
                        <h1><b>@lang('trans.jargon')</b></h1>
                    </div>
                    <div class="cta-intro">
                        <a href="#contactus">@lang('trans.contactnow')</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                        <img src="{{asset('assets/img/hero.png')}}" class="img-fluid hero">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="skills" style="background-color: #fff;">
    <div class="container">
        <div class="row">      
            <div class="col-lg-4 col-md-6">
                <div class="skills">
                    <div class="title-section-main"></div>
                    <div class="services-detail services-detail-box">
						<i class="fa fa-check-circle wow fadeIn" style="visibility: visible; animation-name: fadeIn; color: #2ea5dc;"></i>
						<p>
							@lang('trans.lab')		
						</p>
						<p>
							@lang('trans.labket').		
						</p>
					</div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="skills">
                    <div class="title-section-main"></div>
                    <div class="services-detail services-detail-box">
						<i class="fa fa-check-circle wow fadeIn" style="visibility: visible; animation-name: fadeIn; color: #2ea5dc;"></i>
						<p>
							@lang('trans.user')		
						</p>
						<p>
							@lang('trans.userket').			
						</p>
					</div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="skills">
                    <div class="title-section-main"></div>
                    <div class="services-detail services-detail-box">
						<i class="fa fa-check-circle wow fadeIn" style="visibility: visible; animation-name: fadeIn; color: #2ea5dc;"></i>
						<p>
							@lang('trans.partner')		
						</p>
						<p>
							@lang('trans.partket').			
						</p>
					</div>
                </div>
            </div>

            </div>
        </div>
</section>
<section id="heroes" style="padding-top:30px;">
    <div class="container">

        <!-- About -->
                
        <div class="row">
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/quote.png')}}" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="padding-top: 30%;">
                    <p class="quote"><b>@lang('trans.harm') <span class="tagline">"Digilab Era"</span></b></p>
            </div>
            </div>
        </div>	
        <br>
        <!-- End About -->
    </div>
</section>
<section id="about">
    <div class="container">
        <div class="title">
            <p><span class="thin-text"> @lang('trans.about')</span> AdamLabs </p>
        </div>
        <hr>
        </div>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="visibility: visible; animation-name: fadeInRight; text-align:justify;">
                    <p></p>
                    <p> @lang('trans.aboutket').</p>
                    <br>
                    <p>@lang('trans.visimisiket').</p>
                    <br>
                    <p> @lang('trans.quote')</p>
                    <br><p><span>@lang('trans.yth')</span></p>
                    <p style="margin-top:30px;margin-bottom:30px;"><i><span>@lang('trans.ind')</span><i></p>
                    <div style="padding-top: 15px">
                    <a class="btn btn-md" href="{{$locale}}/about" style="font-size: 13px;
                        font-weight: 600;
                        text-transform: uppercase;
                        color: #fff;
                        background: #2ea5dc;
                        padding: 11px 35px;
                        border-radius: 50px;
                        border: 1px solid #2ea5dc;
                        margin-right: 10px;">@lang('trans.link')  <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/UI-ADAMLABS-new.png')}}" class="img-fluid">
                </div>
            </div>
        </div>	
        <br>
        <!-- End About -->
    </div>
</section>
{{-- <section id="about" style="background-color:#e6e8ea;">
    <div class="container">
        <div class="title">
            <p><span class="thin-text">@lang('trans.visimisi')</span> AdamLabs </p>
        </div>
        <!-- About -->
                    <hr>
                    <div class="about-list">	
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p><b>@lang('trans.akurasi')</b></p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('trans.layanan')</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('trans.integrasi')</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('trans.platform')</p>
                        </div>
                    </div>
        <!-- End About -->
    </div>
</section> --}}
<section id="features" style="padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="title">
            <p>@lang('trans.pelayanan')</p>
        </div>
        <hr>
        <h4><i class="fa fa-info-circle wow fadeIn" style="visibility: visible; animation-name: fadeIn; color:red;"></i>  @lang('trans.p')</h4>
        <hr>
        <div class="row">
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/question.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.p1')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/server.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.p2')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/24-hours-support.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.p3')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/healthcare-and-medical.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.p4')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/user.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.p5')</p>
                </div>
            </div>
        </div>
        <h4><i class="fa fa-check-circle wow fadeIn" style="visibility: visible; animation-name: fadeIn; color:green;"></i>  @lang('trans.s')</h4>
        <hr>
        <div class="row">
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/heart-rate-monitor.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s1')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/lab.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s2')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/report.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s3')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/code.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s4')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/files-and-folders.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s5')</p>
                </div>
            </div>
        </div>
        <h4><i class="fa fa-money wow fadeIn" style="visibility: visible; animation-name: fadeIn; color:green;"></i>  @lang('trans.b')</h4>
        <hr>
        <div class="row">
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/iso.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s6')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/trash.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s7')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/maintenance.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s8')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/cost.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s9')</p>
                </div>
            </div>
            <div class="col-md-5 col-lg-2">
                <div class="features-detail">
                    {{-- <i class="fa fa-cube wow fadeIn" style="visibility: visible; animation-name: fadeIn;"></i> --}}
                    <img src="{{asset('assets/img/refresh.png')}}" style="width: 70px">
                    <hr>
                    <p>@lang('trans.s10')</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="blog">
    <div class="container">
        <div class="title">
            <p>@lang('trans.blog')</p>
        </div>
        <div class="blog-detail">
            <div class="row">
                @foreach ($blog as $post)
                    
                <div class="col-lg-4 col-md-6">
                    <div class="blog-box">
                        <div class="blog-img">
                            <a href="">
                                <img src="{{asset('assets/img/blog/2.jpg')}}" alt="blog">
                                <span class="blog-img-date">{{ $post->created_at->diffForHumans()}}</span>
                            </a>
                        </div>
                        <div class="blog-box-title">
                            <a href="">
                                {{ \Illuminate\Support\Str::limit(strip_tags($post->title),50)}}
                            </a>
                        </div>
                        <div class="blog-description">
                            <p>
                                {{ \Illuminate\Support\Str::limit(strip_tags($post->content),100)}}
                            </p>
                        </div>
                        <a href="/post/{{$post->slug}}" class="btn btn-sm" style="background-color: #151945; color:white;"> @lang('trans.link') <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="pull-right">
            <a href="{{$locale}}/blog" class="btn btn-md" style="background-color: #2ea5dc; color:white;">@lang('trans.posts')</a>
            </div>
        </div>
    </div>
</section>
<section id="contactus"  style="background-color:#e6e8ea;">
    <div class="container">
        <div class="title">
            <p>@lang('trans.kontak')</p>
        </div>
        <hr>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="text-center">
                    <form action="#" method="post" id="" class="" target="_blank">
                        <div class="row">
                            <div class="col-md-12" style="padding-top:5px;">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Masukan Nama">
                            </div>
                            <div class="col-md-12" style="padding-top:5px;">
                                <input type="text" name="email" id="email" class="form-control" placeholder="Masukan Email *" required="required">
                            </div>
                            <div class="col-md-12" style="padding-top:5px;">
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Masukan Judul *" required="required">
                            </div>
                            <div class="col-md-12" style="padding-top:5px;">
                                <textarea name="message" id="message" class="form-control" placeholder="Pesan Anda" required="" style="padding-top:5px;"></textarea>
                            </div>
                            <div class="col-md-12 text-center" style="padding-top:5px;">
                                <input type="submit" name="submit" class="btn btn-block" style="background-color: #151945; color:white;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/contactus.png')}}" width="350px" class="img-fluid">
                </div>
            </div>
        </div>	
        <!-- End About -->
    </div>
</section>
@endsection
