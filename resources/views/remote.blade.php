@extends('layouts.index')
@section('title')
Bantuan Onsite ADAMLabs 
@endsection
@section('content')
<section id="about" style="background-color: #fafafa;">
    <div class="container">
        <div class="title">
            <p><span class="thin-text"> Bantuan</span> Onsite/Kunjungan</p>
        </div>
        <hr>
        <div class="text-center">
           <p> Untuk menjaga komitmen kita kepada pelanggan terkait teknis LIS, berikut langkah-langkah untuk menyampaikan keluhan.</p>
        </div>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <p>Checklist <b>Form</b></p>
                    <hr>
                    {{-- <p>Dengan didukung oleh tim yang berpengalaman,
                        kami berusaha untuk memenuhi segala kebutuhan laboratorium agar layanan kesehatan menjadi lebih baik. <b> Integrasi</b>. merupakan kata yang menggambarkan visi kami di masa depan.</p> --}}
                    <div class="about-list">	
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p><b>Isi data pada kolom yang sudah di sediakan</b></p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>Sampaikan keluhan secara detail dan rinci agar mudah di pahami</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>Sertakan foto atau file untuk format yang di inginkan</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>Tunggu sampai dapat tanggapan dari team</p>
                        </div>
                            <a class="btn btn-md" href="#" style="font-size: 13px;
                            font-weight: 600;
                            text-transform: uppercase;
                              color: #fff;
                              background: #2ea5dc;
                              padding: 11px 35px;
                              border-radius: 50px;
                              border: 1px solid #2ea5dc;
                            margin-right: 10px;">Buka Form Bantuan  <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/remote.jpg')}}" width="400px;" class="img-fluid">
                </div>
            </div>
        </div>	
        <br>
        <div class="text-center">
            <p> Sebagai penyedia pertama layanan sistem laboratorium rumah sakit yang terintegrasi, kami berharap dapat berkolaborasi dengan anda untuk meningkatkan kualitas layanan kesehatan di Indonesia sesuai dengan agenda Sustainable Development Goals #3.</p>
            <br><p><span>Sincerely, Youth Indonesian.</span></p>
         </div>		
        <!-- End About -->
    </div>
</section>
@endsection
