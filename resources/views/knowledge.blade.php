@extends('layouts.index')
@section('title')
Petunjuk & Panduan ADAMLabs  
@endsection
@section('content')
<section class="header-detail">
    <div class="container">
        <div class="header-title">
            <p>Knowledge Base</p>
        </div>
    </div>
</section>
<section id="page-video">
    <div class="container-fluid">
        <div class="row">
            @foreach ($kb as $item)
            <div class="col-lg-3 col-md-6">
                <div class="page-video-box">
                    <a href="/knowledge/{{$item->slug}}">
                        <div class=""> 
                            {{-- <iframe width="100%" height="188" src="https://www.youtube.com/embed/2ASq2Oz0TiI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe> --}}
                            <img src="{{ $item->getGambar()}}" alt="blog">
                        </div>
                        <div class="page-video-box-text">
                            <div class="page-video-box-category">
                                {{$item->created_at}}</div>
                            <div class="page-video-box-title">
                                {{$item->title}}</div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
