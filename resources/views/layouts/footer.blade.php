<section class="footer-new">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="footer-new-detail">
                    <div class="footer-new-title">
                        @lang('footer.playstore') :
                    </div>
                    <div class="footer-new-desc fnd">
                        <div class="row">
                            <div class="col-sm-6" style="padding-top: 10px;">
                                <img src="{{asset('assets/img/googleplay.png')}}" alt="">
                            </div>
                            <div class="col-sm-6" style="padding-top: 10px;">
                              <img src="{{asset('assets/img/appstore.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-social-media">
                    <div class="footer-new-title">
                        @lang('footer.sosmed') :
                    </div>
                    <div class="footer-social-media-detail">
                        <ul>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook fa-3" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-github" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    {{-- <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-new-detail fnt-mobile">
                            <div class="footer-new-title">
                                @lang('footer.produk')
                            </div>
                            <div class="footer-new-desc">
                                <ul>
                                    <li><a href="index">LIS</a></li>
                                    <li><a href="index">SIMRS</a></li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-new-detail fnt-mobile">
                            <div class="footer-new-title">
                                @lang('footer.bantuan')</div>
                            <div class="footer-new-desc">
                                @php $locale = app()->getLocale(); @endphp
                                {{-- {{dd($locale)}} --}}
                                <ul>
                                <li><a href="{{$locale}}/keluhan">@lang('footer.ticket')</a></li>
                                    <li><a href="{{$locale}}/permintaan">@lang('footer.req')</a></li>
                                    {{-- <li><a href="{{url('/support/remote')}}">bantuan remote</a></li>
                                    <li><a href="{{url('/support/onsite')}}">bantuan onsite</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-new-detail fnt-mobile">
                            <div class="footer-new-title">
                                @lang('footer.kb')</div>
                            <div class="footer-new-desc">
                                <li><p> Mengatasi Jika Rasberry Mati</p></li>
                                <li><p> Mengatasi Jika Tidak Bisa Print</p></li>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-new-detail fnt-mobile">
                            <div class="footer-new-title">
                                @lang('footer.kontak')</div>
                            <div class="footer-new-desc">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i> Jl, Bogorami Regency Blok B 21, Surabaya, Jawa Timur, Indonesia
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class="fa fa-phone" aria-hidden="true"></i> (031) 999 220 22
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class="fa fa-envelope" aria-hidden="true"></i> info@adamlabs.id
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
    </div>
    <div id="copyright">
        <div class="container copyright-detail">
        <p>© {{date('Y')}} Copyright <a href="{{url('/')}}">Adamlabs</a> All rights reserved.</p>
        </div>
    </div>
</section>