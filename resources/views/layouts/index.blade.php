<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset=utf-8>
	<meta name=description content="Personal Portfolio Template from seniman koding">
	<meta name="keyword" content="app, landing, software, html, responsive">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<!-- Title -->
  <title>@yield('title')</title>
  	{{-- <!-- Vue -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
	<!-- Favicon -->
	<link rel="icon" type="img/png" sizes="32x32" href="{{asset('/assets/img/adam.png')}}">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/bootstrap.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/font-awesome.min.css')}}">
	<!-- Main Style -->
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/style.css')}}">
	<!-- Responsive -->
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/responsive.css')}}">
<style>
    #button {
  display: inline-block;
  background-color: transparent;
  width: 50px;
  height: 50px;
  text-align: center;
  border-radius: 4px;
  position: fixed;
  bottom: 30px;
  right: 30px;
  transition: background-color .3s, 
    opacity .5s, visibility .5s;
  opacity: 0;
  visibility: hidden;
  z-index: 1000;
}
#button::after {
  content: "\f077";
  font-family: FontAwesome;
  font-weight: normal;
  font-style: normal;
  font-size: 2em;
  line-height: 50px;
  color: #2ea5dc;
}
#button:hover {
  cursor: pointer;
  background-color: #333;
}
#button:active {
  background-color: #555;
}
#button.show {
  opacity: 1;
  visibility: visible;
}

/* Styles for the content section */

.content {
  width: 77%;
  margin: 50px auto;
  font-family: 'Merriweather', serif;
  font-size: 17px;
  color: #6c767a;
  line-height: 1.9;
}
@media (min-width: 500px) {
  .content {
    width: 43%;
  }
  #button {
    margin: 30px;
  }
}
.content h1 {
  margin-bottom: -10px;
  color: #03a9f4;
  line-height: 1.5;
}
.content h3 {
  font-style: italic;
  color: #96a2a7;
}
</style>
</head>
<body>
	@include('layouts.nav')
    @yield('content')
    @include('layouts.footer')
    <a id="button"></a>
	<!-- ================= Start WhatsApp==== -->
<a class='fa fa-whatsapp fa-10x animated infinite rubberBand' href='javascript:void(0);' id='wa-icon' onclick='openModal()' style="font-size: 24px;"></a>
<div class='pelajar' id='pelajar'>
      </div>
      <div id='whatsapp'>
      <span class='wa-x' onclick='closeModal()'></span>
      <p class='wa-title'>Kirim Pesan Anda Dengan Form Whatsapp</p>
      <div class='wa-body'>
      <input class='tujuan' type='hidden' value='082287777221'/>
      <!-- No. WhatsApp -->
      <input class='nama wa-input bagi' placeholder='Nama Lengkap..' type='text'/>
      <input class='email wa-input bagi' placeholder='Alamat E-mail..' type='email'/>
      <textarea class='pesan wa-input full' placeholder='Pesan..'></textarea>
      <a class='submit'>Kirim</a>
      </div>
  </div>
  {{-- <!--Vue-->
  <script src="{{ asset('js/app.js') }}"></script> --}}
	<!--Jquery js-->
    <script src="{{asset('assets/js/jquery-3.0.0.min.js')}}"></script>
    <!--Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Smooth Scroll -->
    <script src="{{asset('assets/js/smooth-scroll.js')}}"></script>
    <!--Main js-->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        var btn = $('#button');

        $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
        });

        btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
        });


    </script>

    <script type='text/javascript'>
        function closeModal() {
        document.getElementById('pelajar').classList.remove('active')
        document.getElementById('whatsapp').classList.remove('active')
        }
        function openModal() {
        document.getElementById('pelajar').classList.add('active')
        document.getElementById('whatsapp').classList.add('active')
        }
        // Onclick Whatsapp Sent!
        $('#whatsapp .submit').click(WhatsApp);
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        function WhatsApp() {
            var ph = '';
            if ($('#whatsapp .nama').val() == '') { // Cek Nama
                ph = $('#whatsapp .nama').attr('placeholder');
                alert('Silahkan tulis ' + ph);
                $('#whatsapp .nama').focus();
                return false;
            } else if ($('#whatsapp .email').val() == '') { // Cek Email
                ph = $('#whatsapp .email').attr('placeholder');
                alert('Silahkan tulis ' + ph);
                $('#whatsapp .email').focus();
                return false;
            } else if (reg.test($('#whatsapp .email').val()) == false) { // Cek Validasi Email
                alert('Alamat E-mail tidak valid.');
                $('#whatsapp .email').focus();
                 return false;
            } else if ($('#whatsapp .pesan').val() == '') { // Cek Pesan
                ph = $('#whatsapp .pesan').attr('placeholder');
                alert('Silahkan tulis ' + ph);
                $('#whatsapp .pesan').focus();
                return false;
            } else {
                if (!confirm("Sudah menginstall WhatsApp?")) {
                    window.open("https://www.whatsapp.com/download/");
                } else {
                    // Check Device (Mobile/Desktop)
                    var url_wa = 'https://web.whatsapp.com/send';
                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        url_wa = 'whatsapp://send/';
                    }
                    // Get Value
                    var tujuan = $('#whatsapp .tujuan').val(),
                            via_url = location.href,
                            nama = $('#whatsapp .nama').val(),
                            email = $('#whatsapp .email').val(),
                            pesan = $('#whatsapp .pesan').val();
                    $(this).attr('href', url_wa + '?phone=62 ' + tujuan + '&text=Halo, saya *' + nama + '* (' + email + ').. %0A%0A' + pesan + '%0A%0Avia ' + via_url);
                    var w = 960,
                            h = 540,
                            left = Number((screen.width / 2) - (w / 2)),
                            tops = Number((screen.height / 2) - (h / 2)),
                            popupWindow = window.open(this.href, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=1, copyhistory=no, width=' + w + ', height=' + h + ', top=' + tops + ', left=' + left);
                    popupWindow.focus();
                    return false;
                }
            }
        }
        </script>
        <!-- ================= End WhatsApp==== -->
</body>
</html>