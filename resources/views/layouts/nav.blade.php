<nav id="navbar" class="navbar navbar-expand-md navbar-medisancis fixed-top">
    <div class="container">
        <a class="navbar-brand" href="en" id="">
            <img src="{{asset('assets/img/ADAMLABS-01.png')}}">
        </a> 
        @php $locale = session()->get('locale'); @endphp
        <div class="nav-mobile-language">
            <ul class="navbar-right">
                <li class="nav-domain">
                    <a class="nav-link" href="{{url('/')}}" id=""> 
                        <span>
                            <img src="{{asset('assets/img/id.png')}}"> ID
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </a>
                    <ul>
                        <li>
                        <a href="{{url('/')}}"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li>
                            <a href="{{url('/')}}"><img src="{{asset('assets/img/logo_adamlabs.png')}}"> English</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-url none-nav">
                    <a class="nav-link" href="{{url('/')}}" id=""> 
                        <span>
                            <img src="{{asset('assets/img/en.png')}}"> EN  
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </a>

                    <ul class="ul-all">
                        <li>
                            <a href="{{url('/')}}"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li class="domain-no">
                            <a href="{{url('/')}}"><img src="{{asset('assets/img/en.png')}}"> English</a>
                        </li>
                    </ul>

                    <ul class="ul-detail none-nav">
                        <li>
                            <a href="{{url('/')}}"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li class="domain-no">
                            <a href="{{url('/')}}"><img src="{{asset('assets/img/en.png')}}"> English</a>
                        </li>
                    </ul>
                </li> 
            </ul>
        </div>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="{{url('/en')}}" id="">@lang('navbar.home')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="about-link" href="#about">@lang('navbar.about')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="services-link" href="#features">@lang('navbar.services')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="portfolio-link" href="#blog">@lang('navbar.article')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contactus">@lang('navbar.contact')</a>
                </li>
                @php $locale = app()->getLocale(); @endphp
                <li class="nav-desktop-language nav-domain">
                    <a class="nav-link" href="#!" id=""> 
                        @switch($locale)
                        @case('en')
                        <span>
                            <img src="{{asset('assets/img/en.png')}}"> EN  
                            <i class="fa fa-angle-down"></i>
                        </span>
                        @break
                        @default
                        <span>
                            <img src="{{asset('assets/img/id.png')}}"> ID 
                            <i class="fa fa-angle-down"></i>
                        </span> 
                        @endswitch
                    </a>

                    <ul>
                        <li class="domain-ok">
                            <a href="id"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li class="domain-ok">
                            <a href="en"><img src="{{asset('assets/img/en.png')}}"> English</a>
                        </li>
                    </ul>
                </li> 

                <li class="nav-desktop-language nav-url none-nav">
                    <a class="nav-link" href="id" id=""> 
                        <span>
                            <img src="{{asset('assets/img/id.png')}}"> ID 
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </a>

                    <ul class="ul-all">
                        <li>
                            <a href="id"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li class="domain-no">
                            <a href="en"><img src="{{asset('assets/img/en.png')}}"> English</a>
                        </li>
                    </ul>

                    <ul class="ul-detail none-nav">
                        <li>
                            <a href="id"><img src="{{asset('assets/img/id.png')}}"> Indonesia</a>
                        </li>
                        <li class="domain-no">
                            <a href="en"><img src="{{asset('assets/img/en.png')}}"> English</a>
                        </li>
                    </ul>

                </li> 
            </ul>
        </div>
    </div>
</nav>