<!doctype html>
<html lang="en">
<head>
    <title>Dashboard @yield('title')</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <!-- jQuery UI required by datepicker editable -->
  <link href="{{asset('admin/dist/assets/plugins/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('admin/dist/assets/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('admin/dist/assets/plugins/bootstrap-markdown/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />

  <!-- JQVMap css -->
  <link href="{{asset('admin/dist/assets/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet" type="text/css" />

  <!-- Bootstrap Tour css -->
  <link href="{{asset('admin/dist/assets/plugins/bootstrap-tour/bootstrap-tour-standalone.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Uploader Styles -->
    <link href="{{asset('admin/dist/assets/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- App css -->
  <link href="{{asset('admin/dist/assets/css/bootstrap-custom.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('admin/dist/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css" />
  

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{asset('assets/img/adam.png')}}">
  <style>
    #pdf{ width:100%; float:left; height:auto; border:1px solid #5694cf;}
    </style>
    <livewire:styles>
</head>
<body>
  <livewire:scripts>
  <!-- WRAPPER -->
  <div id="wrapper">

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand fixed-top" style="background-color: #ffff;">
      <div class="navbar-brand d-none d-lg-block">
        <a href="#"><img src="{{asset('assets/img/ADAMLABS-01.png')}}" width="200px" height="50px"></a>
      </div>
      <div class="container-fluid p-0">
        <button id="tour-fullwidth" type="button" class="btn btn-default btn-toggle-fullwidth" style="position:center; padding-left:50px;"><i class="ti-menu"></i></button>
        <div id="navbar-menu">
          <ul class="nav navbar-nav align-items-center">
          <li><a href="{{route('logout')}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </div>
      </div>
    </nav>
    
    <!-- END NAVBAR -->

    <!-- LEFT SIDEBAR -->
    <div id="sidebar-nav" class="sidebar">
      <!-- LEFT SIDEBAR -->

<nav>
    <ul class="nav">
        <li><a href="{{url('/admin/dashboard')}}" class="{{set_active('dashboard')}}"> <i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="{{url('/admin/knowledge')}}" class="{{set_active('knowledge')}}"> <i class="ti-layout-media-right-alt"></i> <span>Knowledge</span></a></li>
        <li><a href="{{url('/admin/artikel')}}" class="{{set_active('artikel')}}"> <i class="ti-wordpress"></i> <span>Artikel</span></a></li>
        {{-- <li><a href="{{url('/admin/galeri')}}" class="{{set_active('galeri')}}"> <i class="ti-gallery"></i> <span>Galery</span></a></li> --}}
        <li><a href="{{url('/admin/keluhan')}}" class="{{set_active('keluhan')}}"> <i class="ti-receipt"></i> <span>Keluhan</span></a></li>
        <li><a href="{{url('/admin/permintaan')}}" class="{{set_active('permintaan')}}"> <i class="ti-clipboard"></i> <span>Permintaan</span></a></li>
        {{-- <li><a href="{{url('/admin/setting')}}" class="{{set_active('setting')}}"> <i class="ti-panel"></i> <span>Setting</span></a></li> --}}
    </ul>
</nav>
<!-- END LEFT SIDEBAR -->
    </div>
    <!-- END LEFT SIDEBAR -->
    <div class="main">
      @yield('content')
      {{-- <div class="content-heading">
        <div class="container-fluid">
          
        </div>
      </div> --}}
    </div>

    <div class="clearfix">
    
    <!-- footer -->
    {{-- @include('back.footer') --}}
    <!-- end footer -->
  </div>
  </div>
  <!-- END WRAPPER -->

  <!-- Vendor -->
  <script src="{{asset('admin/dist/assets/js/vendor.min.js')}}"></script>

  <!-- Plugins -->
  <script src="{{asset('admin/dist/assets/plugins/jquery-jeditable/jquery.jeditable.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery.maskedinput/jquery.maskedinput.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery-jeditable/jquery.jeditable.masked.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script> <!-- required by datepicker plugin -->
  <script src="{{asset('admin/dist/assets/plugins/jquery-jeditable/jquery.jeditable.datepicker.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/justgage/raphael.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/justgage/justgage.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/datatables.net/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery.appear/jquery.appear.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('admin/dist/assets/plugins/bootstrap-tour/bootstrap-tour-standalone.js')}}"></script>

  <!-- Init -->
  {{-- <script src="{{asset('admin/dist/assets/js/pages/ui-dragdroppanel.init.min.js')}}"></script>
  <script src="{{asset('assets/js/app.js')}}"></script>
  <script src="{{asset('assets/js/moment.js')}}"></script>
  <script src="{{asset('assets/js/local-moment.js')}}"></script> --}}
  <!-- App -->
  <script src="{{asset('admin/dist/assets/js/app.min.js')}}"></script>

  <script src="{{asset('admin/dist/assets/plugins/summernote/summernote-bs4.min.js')}}"></script>


    <!-- Markdown Plugins -->
    <script src="{{asset('admin/dist/assets/plugins/markdown/markdown.js')}}"></script>
    <script src="{{asset('admin/dist/assets/plugins/to-markdown/to-markdown.js')}}"></script>
    <script src="{{asset('admin/dist/assets/plugins/bootstrap-markdown/bootstrap-markdown.js')}}"></script>


      <!-- Uploader Plugin -->
  <script src="{{asset('admin/dist/assets/plugins/dropify/js/dropify.min.js')}}"></script>

  <!-- Uploader Init -->
  <script src="{{asset('admin/dist/assets/js/pages/forms-dragdropupload.init.min.js')}}"></script>
    <!-- Toaster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script> --}}
    <script>
      $('#message').summernote({
       height: 200,
       codemirror: {
           theme: 'monokai'
       },
       callbacks: {
         onChange: function (contents, $editable) {
           window.livewire.emit('textArea', contents, $editable)
         }
       }
   });
  </script>   
</body>
</html>