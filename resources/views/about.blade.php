@extends('layouts.index')
@section('title')
About ADAMLabs
@endsection
@section('content')
<section id="about">
    <div class="container">
        <div class="title">
            <p><span class="thin-text"> @lang('trans.about')</span> AdamLabs </p>
        </div>
        <hr>
        <div class="text-center">
           <p> @lang('trans.aboutket').</p>
        </div>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <p></p>
                    <p> @lang('trans.quote')</p>
                    <br><p><span>@lang('trans.yth').</span></p>
                    <p style="margin-top:30px;margin-bottom:30px;"><i><span>@lang('trans.ind')</span><i></p>
                </div>
            </div>
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/aboutus.png')}}" class="img-fluid">
                </div>
            </div>
        </div>	
        <br>
        <!-- End About -->
    </div>
</section>
@endsection
