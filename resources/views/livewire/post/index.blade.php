<div>
    <div class="content-heading">
        @if ($statusUpdate)
        <livewire:post.update>
        @else
        <livewire:post.create>
        @endif
    </div>
    @if (session()->has('message'))
    <div class="alert badge-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><span class="fa fa-check-circle fa-lg"></span> {{session('message')}}</strong>
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-4">
                    <select wire:model="paginate" name="" id="" class="form-control-sm">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>

                <div class="col-sm-4">
                    <input wire:model="search" type="text" class="form-control-md" placeholder="Cari Disini ...">
            </div>
                <div class="col-sm-4">
                    <button wire:click="refresh()" class="btn btn-sm btn-primary" class="fa fa-reload"> Refresh</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="media mt-3 mb-3">
                <div class="media-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Status</th>
                                <th>Publish</th>
                                <th>Update</th>
                                {{-- <th>Author</th> --}}
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($post as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{ \Illuminate\Support\Str::limit(strip_tags($item->title),30)}}</td>
                                <td>{{$item->categories['category']}}</td>
                                @if ($item->status === 'published')
                                <td><span class="badge badge-success">{{$item->status}}</span></td>
                                @else
                                <td><span class="badge badge-info">{{$item->status}}</span></td>
                                @endif
                                <td>{{$item->created_at->diffForHumans()}}</td>
                                <td>{{$item->updated_at->diffForHumans()}}</td>
                                {{-- <td>{{$item->user['name']}}</td> --}}
                                <td>
                                    <a target="_blank" href="{{url('post')}}/{{$item->slug}}" class="btn btn-sm btn-primary"><i class="ti-eye"></i></a>
                                    @if ($publish === $item->status)
                                    <button wire:click="draftPost({{$item->id}})" class="btn btn-sm btn-info"><i class="ti-archive"></i></button>
                                    @else
                                    <button wire:click="publishPost({{$item->id}})" class="btn btn-sm btn-success"><i class="ti-announcement"></i></button>
                                    @endif
                                    <button wire:click="edit({{$item->id}})" class="btn btn-sm btn-warning"><i class="ti-pencil-alt"></i></button>
                                    @if ($confirming===$item->id)
                                    <button wire:click="hapus({{$item->id}})" class="btn btn-sm btn-danger"><i class="fa fa-info"></i> Apa Anda Yakin ?</button>
                                    @else
                                    <button wire:click="confirmDelete({{$item->id}})" class="btn btn-sm btn-danger"><i class="ti-trash"></i></button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{$kb->links()}} --}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>