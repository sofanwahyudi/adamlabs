<div class="container">
    <div wire:ignore.self id="myAddModal" class="modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                    <div class="box box-warning">
                        <div class="modal-body">
                            <div class="box-body">
                                <form wire:submit.prevent="addKategori">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if (session()->has('message'))
                                            <div class="alert badge-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong><span class="fa fa-check-circle fa-lg"></span> {{session('message')}}</strong>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="" class="control-label">Nama Kategori</label>
                                                <div class="input-group">
                                                    <input wire:model="kat" type="text" name="kategori" id="name" class="form-control" placeholder="Masukan Nama Kategori" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-md btn-primary"> Simpan</i></button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                        </div>
                                </form>
                            <!-- End Form -->
                            </div><!-- box-body-->
                        </div><!-- modal-body-->
                    </div><!-- box-warning-->
            </div><!--md-content-->
        </div><!--md-dialog-->
    </div>
    <form wire:submit.prevent="store">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Judul Artikel') !!}
                    <input wire:model="title" class="form-control" placeholder="Judul" type="text" name="" id="">
                    @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Kategori Artikel') !!}
                    <a data-toggle="modal" data-target="#myAddModal" href="#" class="fa fa-plus-circle" style="padding-left:10px;"></a>
                    <div class="input-group">
                        <select wire:model="kategori" id="kategori" class="form-control select" name="kategori">
                            @foreach (\App\Model\Category::all() as $jp)
                            <option value="{{$jp->id}}">{{$jp->category}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('kategori') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div wire:ignore>
                <div class="form-group">
                    {!! Form::label('Isi Konten') !!}
                    <textarea wire:model.debounce.500ms="content" class="form-control" type="text" name="" id="message"></textarea>
                </div>
                @error('content') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            </div>
            <div class="col-md-6">
                <section>
                    <input type="file" id="image" wire:change="$emit('fileChoosen')">
                    @if($image)
                    <img src={{$image}} width="200" />
                    @endif
                </section>
            </div>
            <div class="col-md-6">
                {!! Form::submit('Post', ['class' => 'btn btn-block btn-primary']) !!}
                {{-- <button wire:click="$emit('postAdded')"> --}}
            </div>
        </div>
    </form>
    
</div>

<script>
    window.livewire.on('fileChoosen', () => {
        let inputField = document.getElementById('image')
        let file = inputField.files[0]
        let reader = new FileReader();
        reader.onloadend = () => {
            window.livewire.emit('fileUpload', reader.result)
        }
        reader.readAsDataURL(file);
    })
</script>
<script>
window.livewire.on('clear', () => {
        let value = document.getElementById('message')
        let v = value.val('')
        reader.onloadend = () => {
            window.livewire.emit('clear', v)
        }
        
    })
</script>