<div class="container">
    <form wire:submit.prevent="update">
        <input type="hidden" name="" wire:model="kbId">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Judul Knowledge') !!}
                    <input wire:model="title" class="form-control" placeholder="Judul" type="text" name="" id="">
                    @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Kategori Knowledge') !!}
                    <div class="input-group">
                        <select wire:model="kategori" id="kategori" class="form-control select" name="kategori">
                            @foreach (\App\Model\Category::all() as $jp)
                            <option value="{{$jp->id}}">{{$jp->category}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('kategori') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div wire:ignore>
                <div class="form-group">
                    {!! Form::label('Content') !!}
                    <textarea wire:model.debounce.500ms="content" class="form-control" type="text" name="" id="summernote"></textarea>
                </div>
                @error('content') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            </div>
            <div class="col-md-6">
                <section>
                    <input type="file" id="image" wire:change="$emit('fileChoosen')">
                    @if($image != null)
                    <img src="{{asset('storage/' .$image)}}" width="200" />
                    @elseif($image && $image == null)
                        @if ($image)
                        <img src={{$image}} width="200" />
                        @endif
                    @endif
                </section>
            </div>
            <div class="col-md-6">
                {!! Form::submit('Update', ['class' => 'btn btn-block btn-primary']) !!}
                {{-- <button wire:click="$emit('postAdded')"> --}}
            </div>
        </div>
    </form>
</div>
<script>
    window.livewire.on('fileChoosen', () => {
        let inputField = document.getElementById('image')
        let file = inputField.files[0]
        let reader = new FileReader();
        reader.onloadend = () => {
            window.livewire.emit('fileUpload', reader.result)
        }
        reader.readAsDataURL(file);
    })
</script>
<script src="{{asset('admin/dist/assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $('#summernote').summernote({
     height: 200,
     codemirror: {
         theme: 'monokai'
     },
     callbacks: {
       onChange: function (contents, $editable) {
         window.livewire.emit('textArea', contents, $editable)
       }
     }
 });
</script> 