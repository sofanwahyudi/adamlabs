@extends('layouts.admin')
@section('content')
<!-- MAIN CONTENT -->
<div class="main-content">
    <div class="container-fluid">
        <div class="panel-heading">
            <br>
            <h3 class="panel-title">Rekapitulasi</h3>
        </div>
        <div class="row">
            <div class="col-md-3 col-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="widget-metric_6 animate">
                            <span class="icon-wrapper custom-bg-red"><i class="fa fa-edit fa-lg"></i></span>
                            <div class="right">
                                {{-- <span class="value">{{$pharam['lowongan']->count()}} </span> --}}
                                <span class="value">5 </span>
                                <span class="title">Keluhan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="widget-metric_6 animate">
                            <span class="icon-wrapper custom-bg-orange"><i class="fa fa-edit fa-lg"></i></span>
                            <div class="right">
                                {{-- <span class="value">{{$pharam['lowongan']->count()}} </span> --}}
                                <span class="value">5 </span>
                                <span class="title">Permintaan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="widget-metric_6 animate">
                            <span class="icon-wrapper custom-bg-yellow"><i class="fa fa-info fa-lg"></i></span>
                            <div class="right">
                                {{-- <span class="value">{{$pharam['lowongan']->count()}} </span> --}}
                                <span class="value">5 </span>
                                <span class="title">Pending</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="widget-metric_6 animate">
                            <span class="icon-wrapper custom-bg-green"><i class="fa fa-check-circle fa-lg"></i></span>
                            <div class="right">
                                {{-- <span class="value">{{$pharam['lowongan']->count()}} </span> --}}
                                 <span class="value">5 </span>
                                <span class="title">Selesai</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-minimal project-list">
              <thead>
                <tr>
                  <th>TITLE</th>
                  <th>DATE START</th>
                  <th>DAYS TO DEADLINE</th>
                  <th>PROGRESS</th>
                  <th>PRIORITY</th>
                  <th>LEADER</th>
                  <th>STATUS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="#">Spot Media</a></td>
                  <td>18-04-2017</td>
                  <td>12 days</td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar" style="width: 95%;" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100">95%</div>
                    </div>
                  </td>
                  <td><span class="badge badge-warning">MEDIUM</span></td>
                  <td class="leader"><a href="#"><img src="assets/images/user2.png" alt="Avatar" class="avatar rounded-circle"> <span>Michael</span></a></td>
                  <td><span class="badge badge-success">ACTIVE</span></td>
                </tr>
                <tr>
                  <td><a href="#">E-Commerce Site</a></td>
                  <td>24-05-2017</td>
                  <td>30 days</td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar" style="width: 40%;" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">40%</div>
                    </div>
                  </td>
                  <td><span class="badge badge-success">LOW</span></td>
                  <td class="leader"><a href="#"><img src="assets/images/user1.png" alt="Avatar" class="avatar rounded-circle"> <span>Antonius</span></a></td>
                  <td><span class="badge badge-warning">PENDING</span></td>
                </tr>
                <tr>
                  <td><a href="#">Project 123GO</a></td>
                  <td>20-12-2016</td>
                  <td>50 days</td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar" style="width: 65%;" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">65%</div>
                    </div>
                  </td>
                  <td><span class="badge badge-danger">HIGH</span></td>
                  <td class="leader"><a href="#"><img src="assets/images/user1.png" alt="Avatar" class="avatar rounded-circle"> <span>Antonius</span></a></td>
                  <td><span class="badge badge-success">ACTIVE</span></td>
                </tr>
                <tr>
                  <td><a href="#">Wordpress Theme</a></td>
                  <td>05-03-2017</td>
                  <td>40 days</td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar" style="width: 77%;" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100">77%</div>
                    </div>
                  </td>
                  <td><span class="badge badge-warning">MEDIUM</span></td>
                  <td class="leader"><a href="#"><img src="assets/images/user2.png" alt="Avatar" class="avatar rounded-circle"> <span>Michael</span></a></td>
                  <td><span class="badge badge-success">ACTIVE</span></td>
                </tr>
              </tbody>
            </table>
          </div>
    </div>
</div>
@endsection