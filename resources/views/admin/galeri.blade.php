@extends('layouts.admin')
@section('title')
Galeri
@stop
@section('content')
<!-- MAIN CONTENT -->
<div class="main-content">
    <div class="content-heading">
        <div class="heading-left">
            <h1 class="page-title">Static Tables</h1>
            <p class="page-subtitle">Simple tables based on Bootstrap</p>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="#">Parent</a></li>
                <li class="breadcrumb-item active">Current</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Basic Table</h3>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Steve</td>
                        <td>Jobs</td>
                        <td>@steve</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Simon</td>
                        <td>Philips</td>
                        <td>@simon</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Jane</td>
                        <td>Doe</td>
                        <td>@jane</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
@endsection
