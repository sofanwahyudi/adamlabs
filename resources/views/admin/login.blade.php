<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
  <title>Login Adamlabs</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- App css -->
  <link href="{{asset('admin/dist/assets/css/bootstrap-custom.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('admin/dist/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{asset('assets/img/adam.png')}}">
</head>
<body>
  <!-- WRAPPER -->
  <div id="wrapper" class="d-flex align-items-center justify-content-center">
    <div class="auth-box ">
      <div class="left">
        <div class="content">
          <div class="header">
            <div class="logo text-center"><img src="{{asset('assets/img/ADAMLABS-01.png')}}" width="200px;" ></div>
          </div>
          @if ($message = Session::get('success'))
            <div id="toast-container" class="toast-top-right">
                <div class="toast toast-success" aria-live="polite" style="display: block;">
                    <button type="button" class="toast-close-button" role="button">×</button>
                    <div class="toast-message">{{ $message }}</div>
                </div>
            </div>
            @endif
            @if ($errors = Session::get('error'))
            <div class="text-center" style="color: crimson;">
                {{-- <ul>
                    @foreach ($errors as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul> --}}
                <div class="badge">{{ $errors }}</div>
            </div><br/>
            @endif
          <form action="{{route('post.login')}}" method="POST">
            @csrf
            <div class="form-group">
              <label for="signin-email" class="control-label">Email</label>
              <input type="email" class="form-control" placeholder="Email" name="email" required>
            </div>
            <div class="form-group">
              <label for="signin-password" class="control-label">Password</label>
              <input type="password" class="form-control"  placeholder="Password" name="password" required>
            </div>
            <button type="submit" class="btn btn-md btn-block" style="background-color: #2ea5dc; color:#fff;">LOGIN</button>
          </form>
        </div>
      </div>
      <div class="right">

      </div>
    </div>
  </div>
  <!-- END WRAPPER -->
</body>
</html>