@extends('layouts.index')
@section('title')
Permintaan Pelanggan ADAMLabs 
@endsection
@section('content')
<section id="about">
    <div class="container">
        <div class="title">
            <p><span class="thin-text"> @lang('permintaan.title')</p>
        </div>
        <hr>
        <div class="text-center">
           <p> @lang('permintaan.subtitle').</p>
        </div>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <p>@lang('permintaan.h1')</p>
                    <hr>
                    {{-- <p>Dengan didukung oleh tim yang berpengalaman,
                        kami berusaha untuk memenuhi segala kebutuhan laboratorium agar layanan kesehatan menjadi lebih baik. <b> Integrasi</b>. merupakan kata yang menggambarkan visi kami di masa depan.</p> --}}
                    <div class="about-list">	
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p><b>@lang('permintaan.p1').</b></p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('permintaan.p2').</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('permintaan.p3').</p>
                        </div>
                        <div class="about-list-detail">
                            <i class="fa fa-check-circle" style="color: #2ea5dc;"></i>
                            <p>@lang('permintaan.p4').</p>
                        </div>
                            <a data-toggle="modal" data-target="#myAddModal"  class="btn btn-md" href="#" style="font-size: 13px;
                            font-weight: 600;
                            text-transform: uppercase;
                              color: #fff;
                              background: #2ea5dc;
                              padding: 11px 35px;
                              border-radius: 50px;
                              border: 1px solid #2ea5dc;
                            margin-right: 10px;">@lang('permintaan.button') <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            {{-- e6e8ea --}}
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/form_permintaan.jpg')}}" width="400px" class="img-fluid">
                </div>
            </div>
        </div>	
        <br>
        {{-- <div class="text-center">
            <p> Sebagai penyedia pertama layanan sistem laboratorium rumah sakit yang terintegrasi, kami berharap dapat berkolaborasi dengan anda untuk meningkatkan kualitas layanan kesehatan di Indonesia sesuai dengan agenda Sustainable Development Goals #3.</p>
            <br><p><span>Sincerely, Youth Indonesian.</span></p>
         </div>		 --}}
        <!-- End About -->
    </div>
</section>
<!-- Ini awalan modal tambah -->
<div class="box box-warning">
    <div  id="myAddModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                            <div class="box box-warning">
                                <div class="modal-body">
                                    <div class="box-body">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Nama RS</label>
                                                        <div class="input-group">
                                                            <input type="text" name="name" id="name" class="form-control" placeholder="Masukan Nama Rumah Sakit" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Nama Pelapor (IT RS/KALAB/ANALYS)</label>
                                                        <div class="input-group">
                                                            <input type="text" name="name" id="name" class="form-control" placeholder="Masukan Nama Pelaksana" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Keluhan</label>
                                                        <div class="input-group">
                                                            <textarea name="message" id="message" class="form-control" placeholder="Sampaikan Keluhan Anda" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Upload File (Maksimal Ukuran File adalah 2MB, Format yang di ijinkan PDF/JPG/PNG/JPEG)</label>
                                                        <div class="input-group">
                                                            <input type="file" name="name" id="name" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="box-footer">
                                                    <button type="submit" class="btn btn-primary"> Kirim</button>
                                                </div>
                                        </form>
                                    <!-- End Form -->
                                    </div><!-- box-body-->
                                </div><!-- modal-body-->
                            </div><!-- box-warning-->
                    </div><!--md-header-->
                </div><!--md-content-->
            </div><!--md-dialog-->
        </div><!--mydetailmodal-->
        <!-- Ini akhiran modal tambah -->
@endsection
