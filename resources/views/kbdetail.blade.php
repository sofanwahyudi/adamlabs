@extends('layouts.index')
@section('title')
KB {{$kb->title}}
@stop
@section('content')
<section id="blog">
    <div class="container">
        <div class="title">
            <div class="title-section-main">
                {{$kb->title}}
            </div>
            {{-- <div class="title-section-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </div> --}}
        </div>

        <div class="blog-detail">
            <div class="blog-box">
                <div class="blog-img">
                    <a href="">
                        <img src="{{ $kb->getGambar()}}" alt="blog">
                        <span class="blog-img-date">{{ $kb->created_at->diffForHumans()}}</span>
                    </a>
                </div>
                <div class="blog-box-title">
                    <a href="">
                        {{$kb->title}}
                    </a>
                </div>
                <div class="container">
                    <p>
                        {!! $kb->content !!}
                    </p>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection
