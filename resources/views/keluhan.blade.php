@extends('layouts.index')
@section('title')
Keluhan Pelanggan ADAMLabs 
@endsection
@section('content')
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
@if(Session::has('success'))
<div class="alert alert-success">{{ Session::get('success') }}
</div>
@endif
<section id="about">
    <div class="container">
        <div class="title">
            <p><span class="thin-text">@lang('keluhan.title')</p>
        </div>
        <hr>
        <div class="text-center">
           <p> @lang('keluhan.subtitle').</p>
        </div>

        <!-- About -->
                
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <p>@lang('keluhan.h1')</p>
                    <hr>
                    <form id="formkeluhan" action="{{route('testapi')}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama RS</label>
                                        <div class="input-group">
                                            <input type="text" name="ip" id="ip" class="form-control" placeholder="Masukan Nama Rumah Sakit" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Pelapor (IT RS/KALAB/ANALYS)</label>
                                        <div class="input-group">
                                            <input type="text" name="pic" id="pic" class="form-control" placeholder="Masukan Nama Pelaksana" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Keluhan</label>
                                        <div class="input-group">
                                            <textarea name="keluhan" id="keluhan" class="form-control" placeholder="Sampaikan Keluhan Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Upload File (Maksimal Ukuran File adalah 2MB, Format yang di ijinkan PDF/JPG/PNG/JPEG)</label>
                                        <div class="input-group">
                                            <input type="file" name="name" id="file" class="form-control" required>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                                <div class="box-footer">
                                    <button id="btn-add" type="submit" class="btn btn-block btn-primary"> Kirim</button>
                                </div>
                        </form>
                </div>
            </div>
            <div class="col-lg-6" style="padding-top: 5px;">
                <div class="about-img wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <img src="{{asset('assets/img/form_permintaan.jpg')}}" width="550px" class="img-fluid">
                </div>
            </div>
        </div>	
        <br>
    </div>
</section>

@endsection

<!--Jquery js-->
<script src="{{asset('assets/js/jquery-3.0.0.min.js')}}"></script>
<script>
    (function () {
        document.querySelector('#formKeluhan').addEventListener('submit', function (e) {
            e.preventDefault()
            axios.post(this->action, {
                'ip': document.querySelector('#ip').value,
                'pic': document.querySelector('#pic').value,
                'keluhan': document.querySelector('#keluhan').value,
            })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    })
</script>