@extends('layouts.index')
@section('title')
Article ADAMLabs  
@endsection
@section('content')
@php $locale = app()->getLocale(); @endphp
<section id="blog">
    <div class="container">
        <div class="title-section">
            <div class="title-section-main">
                @lang('trans.blog')
            </div>
            {{-- <div class="title-section-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </div> --}}
        </div>

        <div class="blog-detail">
            <div class="row">
                @foreach ($blog as $post)
                    
                <div class="col-lg-4 col-md-6">
                    <div class="blog-box">
                        <div class="blog-img">
                            <a href="">
                                <img src="{{ $post->getGambar()}}" alt="blog">
                                <span class="blog-img-date">{{ $post->created_at->diffForHumans()}}</span>
                            </a>
                        </div>
                        <div class="blog-box-title">
                            <a href="">
                                {{ \Illuminate\Support\Str::limit(strip_tags($post->title),50)}}
                            </a>
                        </div>
                        <div class="blog-description">
                            <p>
                                {{ \Illuminate\Support\Str::limit(strip_tags($post->content),100)}}
                            </p>
                        </div>
                        <a href="/post/{{$post->slug}}" class="btn btn-sm" style="background-color: #151945; color:white;">@lang('trans.link') <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
