<?php
return [
    'title' => 'Halaman Permintaan Fitur',
    'subtitle' => 'Untuk menjaga komitmen kita kepada pelanggan terkait teknis LIS, berikut langkah-langkah untuk menyampaikan keluhan',
    'h1' => 'Form Permintaan',
    'p1' => 'Isi data pada kolom yang sudah di sediakan',
    'p2' => 'Sampaikan permintaan secara detail dan rinci',
    'p3' => 'Sertakan foto atau file untuk format yang di inginkan',
    'p4' => 'Tunggu sampai dapat tanggapan dari team',
    'button' => 'Buka Form Permintaan'
];
?>