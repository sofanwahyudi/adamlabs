<?php
return [
    'home' => 'Beranda',
    'about' => 'Tentang Kami',
    'services' => 'Layanan',
    'article' => 'Artikel',
    'contact' => 'Kontak Kami',
];
?>