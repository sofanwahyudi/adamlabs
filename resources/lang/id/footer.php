<?php
return [
    'sub' => 'Sejalan dengan perkembangan zaman ke Era Informasi Digital, kami sebagai generasi muda Indonesia melalui AdamLabs',
    'produk' => 'Produk',
    'bantuan' => 'Pusat Bantuan',
    'kb' => 'Bantuan Panduan',
    'sosmed' => 'Ikuti Kami di',
    'ticket' => 'Keluhan',
    'req' => 'Permintaan Fitur',
    'playstore' => 'Segera Hadir di',
    'kontak' => 'kontak'
];
?>