<?php
return [
    'jargon' => 'The First Most Integrated Services Of Hospital Laboratory User',
    'welcome' => 'Welcome to',
    'contactnow' => 'Contact Now',
    'harm' => 'New Harmonization Of Digital information and laboratory servicesas we called it ',
    'deskhero' => 'Of Digital information and laboratory servicesas we called it ',
    'lab' => 'Laboratory',
    'user' => 'Users',
    'partner' => 'Partners',
    'labket' => 'Completed and customables software to every laboratory needs',
    'userket' => 'Easily to record your lab’s results and integrated with lab’s system to know more about your lab’s progress',
    'partket' => 'Store your reagents with us and shortly fulfill your customer needs',
    'visimisi' => 'Vision & Mission',
    'visimisiket' => 'With the support of an experienced team, we strive to meet all laboratory needs so that health services become better. Integration. is a word that describes our vision for the future',
    'about' => 'About',
    'aboutket' => 'In line with the development of the era of the Digital Information Age, we as a young generation of Indonesia through AdamLabs, have a mission to encourage health services and quality of services, especially laboratories, to be more optimal by building integrated systems. AdamLabs was founded in 2016 by Wahana Group which is the oldest company engaged in the hospital laboratory business since 1994',
    'akurasi' => 'Data Accuracy',
    'layanan' => '24 hour support',
    'integrasi' => 'Multi Terintegrasi',
    'platform' => 'Multi Platform',
    'link' => 'Read More',
    'posts' => 'More Post',
    'quote' => 'As the first provider of integrated hospital laboratory system services, we look forward to collaborating with you to improve the quality of health services in Indonesia in accordance with the Sustainable Development Goals #3 agenda',
    'yth' => 'Sincerely,',
    'ind' => ' Youth Indonesian',
    'pelayanan' => 'Services',
    'p' => 'Problems that often occurs',
    'p1' => 'Error In Use',
    'p2' => 'Data Security Not Guaranteed',
    'p3' => 'Less Effective & Efficient Services',
    'p4' => 'Subjective Examination Results',
    'p5' => 'Lack of IT Team',
    'p6' => 'Complicated System Processes',
    's' => 'The solution we offer',
    'b' => 'Additional Benefits',
    's1' => 'Laboratory Management and Implementers',
    's2' => 'Report on Laboratory Activities and Patient Medical Records',
    's3' => 'Storage Report System',
    's4' => 'High Advance Technology',
    's5' => 'Triple Backup & Leveling Authorization',
    's6' => 'In Accordance With International System Standards',
    's7' => 'Reducing Paper Use and Environmentally Friendly',
    's8' => 'Ease of Care and Maintenance',
    's9' => 'Guaranteed Service',
    's10' => 'Automatic Updates of Every Feature',
    'blog' => 'Latest News',
    'kontak' => 'Contact',
    'formkontak' => 'Form Contact',
    'ph1' => 'Masukan Nama',
    'ph2' => 'Masukan Email',
    'ph3' => 'Masukan Judul',
    'ph4' => 'Pesan Anda',
];
?>