<?php
return [
    'title' => 'Page Request Features',
    'subtitle' => 'To maintain our commitment to customers regarding technical LIS, here are the steps to make a complaint',
    'h1' => 'Form Request',
    'p1' => 'Fill in the data in the column provided',
    'p2' => 'Make a request in detail and detail',
    'p3' => 'Include photos or files for the desired format',
    'p4' => 'Wait until you get a response from the team',
    'button' => 'Open Form Request'
];
?>