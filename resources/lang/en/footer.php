<?php
return [
    'sub' => 'In line with the times to the Digital Information Age, we as a young generation of Indonesia through AdamLabs',
    'produk' => 'Product',
    'bantuan' => 'Support Center',
    'kb' => 'Knowledge Base',
    'sosmed' => 'Follow Us',
    'ticket' => 'Complaint',
    'req' => 'Request Features',
    'playstore' => 'Available Soon',
    'kontak' => 'Contact Us'
];
?>